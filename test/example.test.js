// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit test mylib.js', () => {

    let myvar = undefined
    let myvar2 = 4

    before(() => {
        myvar = 1;  // setup before testing
        console.log('Before testing...');
    })

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1); // 1 + 1
        expect(result).to.equal(2); // result expected to equal 2
    })

    it('parametrized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar2); // 1 + 4
        expect(result).to.equal(myvar+myvar2);
    })

    it('Assert ripuli is not kakkaa', () => {
        assert('ripuli' !== 'kakkaa') // true
    })

    it('Myvar should exist', () => {
        should.exist(myvar);
    })

    after(() => {
        console.log('After testing...');
    })
})